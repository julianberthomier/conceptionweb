-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 28 Novembre 2021 à 18:08
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `basededonnee_mo_csw`
--

-- --------------------------------------------------------

--
-- Structure de la table `dominante`
--

CREATE TABLE `dominante` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `dominante`
--

INSERT INTO `dominante` (`id`, `nom`) VALUES
(2, 'BDTN'),
(3, 'IA-IR'),
(4, 'ISN'),
(5, 'IF'),
(6, 'ARI'),
(7, 'EDD'),
(8, 'GET'),
(9, 'IA-DES'),
(10, 'MCTGE'),
(11, 'ISE-VA'),
(12, 'ISE-OC'),
(13, 'ISYMED'),
(14, 'ESAA'),
(15, 'ICOM'),
(16, 'CERT');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `dominante`
--
ALTER TABLE `dominante`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `dominante`
--
ALTER TABLE `dominante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
