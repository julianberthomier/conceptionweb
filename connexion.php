<?php
  $titre = "connexion";
  include 'header.inc.php';
  include 'navbar.inc.php';
?>

<div class="container">
<h1>Connexion</h1>
<h6>Veuillez entrer votre identifiant et votre mot de passe pour vous connecter</h6>

<form action="tt_connexion.php" method="POST">
<div class="form-row">
    <div class="col-12 col-md-6 mb-3">
      <input type="email" class="form-control" placeholder="Adresse email" required name="email">
    </div>
    <div class="col-12 col-md-6 mb-3">
      <input type="password" class="form-control" placeholder="Mot de passe" required name="password">
    </div>
</div>

</div>

<div class="form-row">
  <div class="col-2">
    <button type="submit" class="btn btn-primary btn-sm">Se connecter</button>
  </div>
</div>

</form>