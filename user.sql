-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 28 Novembre 2021 à 18:07
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `basededonnee_mo_csw`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` varchar(128) NOT NULL,
  `dominante` int(11) NOT NULL,
  `role` int(50) NOT NULL,
  `sujet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `pass`, `dominante`, `role`, `sujet`) VALUES
(5, 'bdtn', 'bdtn', 'bdtn@gmail.com', '$2y$10$BnlcDTtO0sgeGym9mk147utPjIGitPyf.EbGxwjsA5IvVKtp.L3V.', 2, 1, 6),
(4, 'isn', 'isn', 'isn@gmail.com', '$2y$10$8QA4ceyFX2CkG2J/rLLZl.1Y1IU9dxc3nk44pgeIdkeEhw3k15xlq', 4, 1, NULL),
(1, 'moi', 'moi', 'moi@gmail.com', '$2y$10$/gPylf/kMA7p0EhA0MHpRu6oBOzLgJ5uhJOoiC15b7cVAp9LTGQsa', 2, 1, NULL),
(2, 'prof', 'prof', 'prof@gmail.com', '$2y$10$6He.S.3tW1ri4rq8OtKts..WV192paHQ9/9CgLRvOK5E56FmIcMVG', 2, 2, NULL),
(3, 'toi', 'toi', 'toi@gmail.com', '$2y$10$7xhtOdjuK6Y9h41PIkEfiONLK0TgeHmLHSGbEb9h40rxZA16lhP2G', 2, 1, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
