<?php
session_start();
require_once 'config.inc.php';




if(isset($_POST['email']) && isset($_POST['password']) )
{
    $email = htmlentities($_POST['email']);
    $password = htmlentities($_POST['password']);

    $options = [
        'cost' => 12,
    ];
    
    

    $stmt = $bdd->prepare('SELECT id, prenom, email, pass , role FROM user WHERE email = ?');
    $stmt -> execute(array($email));
    $the_user = $stmt ->fetch();
    $row = $stmt ->rowCount();

    if ($row == 1)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
        {
         
            if(password_verify($password, $the_user['pass']) )
            {
                $_SESSION['message'] = $the_user['prenom'];
                $_SESSION['role'] = $the_user['role'];
                $_SESSION['id'] = $the_user['id'];
                header('Location: index.php?');
                
                
            } else header('Location: connexion.php?login_err=password');

        }else header('Location: connexion.php?login_err=email');

    }else header('Location: connexion.php?login_err=already');

}else header('Location: connexion.php?');




?>