<?php
require_once("param.inc.php");
?>

<nav class="navbar navbar-expand-md navbar-dark bg-dark  mb-3 menu">
    <a  href="https://youtu.be/xvFZjo5PgG0">
    <img src="image\Logo_ESIGELEC.png" class="rounded float-left" height ="25" width="33" href="#"></a>
  <a class="navbar-brand" href="index.php">Esigelec</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
     <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
            <li class="nav-item">
                 <a class="nav-link" href="index.php">Accueil</a>
             </li>
             <?php
            if($_SESSION['role'] == 1){
            echo '
             <li class="nav-item">
                 <a class="nav-link" href="monchoix.php">Mon Choix</a>
             </li>
             ';}
            ?>
      </ul>
      <ul class="navbar-nav">
            <?php
            if($_SESSION['role'] == 2){
            echo '
            <li class="nav-item">
                <a class="nav-link" href="add_subject.php">ajouter un sujet</a>
            </li>';}
            ?>
            <?php
            if($_SESSION['role'] ==0){
            echo '
            <li class="nav-item">
                <a class="nav-link" href="connexion.php">Connexion</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="inscription.php">Inscription</a>
            </li>';}
            ?>
            <?php
            if($_SESSION['role'] !=0){
            echo '
            <li class="nav-item">
                <a class="nav-link" href="deconnexion.inc.php">deconnexion</a>
            </li>';}
            ?>        
      </ul>
  </div>
</nav>

<?php
