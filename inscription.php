<?php
  session_start();
  $titre = "inscription";
  include 'header.inc.php';
  include 'navbar.inc.php';
  require_once 'config.inc.php';
  $stmt = $bdd->prepare('SELECT * FROM dominante WHERE 1');
  $stmt -> execute();
  $doms =  $stmt ->fetchAll();
?>

 <div class="container">
    <h1> Inscription</h1>
    <h6> Saisissez votre identifiant, votre mot de passe et votre dominante pour vous inscrire </h6>

    <form action="tt_inscription.php" method="POST">

  

      <div class="col-12 col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Nom" required name="nom">
      </div>

      <div class="col-12 col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Prenom" required name="prenom">
      </div>


      <div class="form-row">
      <div class="col-12 col-md-6 mb-3">
      <input type="email" class="form-control" placeholder="Adresse email" required name="email">
      </div>

      <div class="col-12 col-md-6 mb-3">
      <input type="password" class="form-control" placeholder="Mot de passe" required name="password">
      </div>

      <div class="col-12 col-md-6 mb-3">
        <select  type="text" class="form-control" placeholder="Dominante" required name="dominante">
        <?php
        foreach ($doms as $dom) {
          echo '<option>  '.$dom['nom'].' </option>';
        }
        
        ?>
        </select>
      </div>

      <div class="form-row">
      <div class="col-2">
      <button type="submit" class="btn btn-primary btn-sm">Envoyer</button>
      </div>


  </div>
</form>


</div>


