<?php
  session_start();
  $titre = "ajouter un sujet";
  include 'header.inc.php';
  include 'navbar.inc.php';
  require_once 'config.inc.php';
  $stmt = $bdd->prepare('SELECT * FROM dominante WHERE 1');
  $stmt -> execute();
  $doms =  $stmt ->fetchAll();
?>
<h1>Ajouter un sujet</h1>

<form action="tt_sujet.php" method="POST" enctype="multipart/form-data" >
    <div class="container">
        <div class="form">
            <div class="col align-self-center">
            <label for="titre">titre</label>
             <input type="text" class="form-control" placeholder="Titre" required name="titre"  >
             </div>
    
             <div class="col align-self-center">
                 <label for="dominant">dominant</label>
                 <select class="form-control" placeholder="Dominante" required name="dom">
                    <?php
                    foreach ($doms as $dom) {
                    echo '<option>  '.$dom['nom'].' </option>';
                     }
                     ?>
                </select>
            </div>
            <div class="col align-self-center">
                <div class="form-group">
                 <label for="description">description</label>
                    <textarea type="text" class="form-control" id="description" rows="3" placeholder="Description" required name="description"></textarea>
                 </div>
             </div>
            <div class="col align-self-center">
                <label for="nombre de place">nombre de place</label>
                <input type="int" class="form-control"  placeholder="Nombre" required name="nombre">
            </div>
            <div class="col align-self-center">
                 <div class="form-group">
                    <label for="formFileMultiple" class="form-label">pdf</label>
                    <input class="form-control" type="file" id="formFileMultiple"  name="pdf">
                 </div>
            </div>

            <div class="col-6">
                <button type="submit" class="btn btn-primary">envoyer sujet</button>
            </div>
          
        </div>
    </div>
</form>