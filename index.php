<?php
  session_start();
  $titre = "Accueil";
  include 'header.inc.php';
  include 'navbar.inc.php';
?>

<?php

  if(isset($_SESSION['message'] ) ) {
    echo '<div class="alert alert-success" role="alert">';
    echo $_SESSION['message'];
    echo '</div>';

    unset($_SESSION['message']);
  }


?>

<h1>Présentation du site</h1>

<p>Au second semestre de leur deuxieme année , les eleves de la filiere classique de l'ESIGELEC doivent effectuer un projet
sur cinq semaines. Chaque departement propose plusieurs sujets. Les eleves doivent
choisir un des sujets parmi ceux proposes par le departement auquel leur dominante est
rattachee. Bienvenu sur le site de réalisation du Projet Semestre 8
</p>
<h1>Sujets Disponibles</h1>
<div class="container">
<?php
require_once("param.inc.php");

if($_SESSION['role'] ==0){
  echo '<h6>Veuillez vous connecter ou vous inscrire pour plus d information</h6>';}
try
{
    $db = new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8', $login, $password);
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
        
}

$sqlQuery ='SELECT * FROM sujet ';
$sql = $db->prepare($sqlQuery);
$sql->execute();
$tests = $sql->fetchAll();
foreach ($tests as $test) {
    include 'sujet.inc.php';
}
?>
</div>

